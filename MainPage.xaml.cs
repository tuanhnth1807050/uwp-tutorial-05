﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using ObservableCollectionExample.Models;
using System.Collections.ObjectModel;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ObservableCollectionExample
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private List<Icon> icons;
        private ObservableCollection<Contact> Contacts;
        public MainPage()
        {
            this.InitializeComponent();

            icons = new List<Icon>();
            icons.Add(new Icon { IconPath = "Assets/Images01/male-01.png" });
            icons.Add(new Icon { IconPath = "Assets/Images01/male-02.png" });
            icons.Add(new Icon { IconPath = "Assets/Images01/male-03.png" });
            icons.Add(new Icon { IconPath = "Assets/Images01/female-01.png" });
            icons.Add(new Icon { IconPath = "Assets/Images01/female-02.png" });
            icons.Add(new Icon { IconPath = "Assets/Images01/female-03.png" });

            Contacts = new ObservableCollection<Contact>();


        }
        private void NewContactButton_click(object sender,RoutedEventArgs e)
        {
            string avatar = ((Icon)AvatarComboBox.SelectedValue).IconPath;
            Contacts.Add(new Contact { FirstName = FirstNameTextBox.Text, LastName = LastNameTextBox.Text, AvatarPath = avatar });

            FirstNameTextBox.Text = "";
            LastNameTextBox.Text = "";
            AvatarComboBox.SelectedIndex = -1;

            FirstNameTextBox.Focus(FocusState.Programmatic);
        }
    }
}
